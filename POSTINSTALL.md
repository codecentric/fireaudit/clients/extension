# Using FireAudit

Whenever a document changes, it’s sent to FireAudit. FireAudit will store it permanently.
You can explore all mutation ever made to any audited document. Next to that, FireAudit tries to find issues in your data quality, for example, missing modifications, or malicious tampering by administrators.
Go to [FireAudit Studio](https://fireaudit.studio) to find out more!

# Advanced usage of FireAudit Client Functions

FireAudit offers more customisation via it's [client library](https://www.npmjs.com/package/@fireaudit/functions).
Additional customisations include any amount of data nesting, data encryption, and data transformation.
For now, Firebase Extensions do not allow these extensive customisations, so if desired, you should deploy them yourself as Firebase Functions.

# Monitoring

As a best practice, you can [monitor the activity](https://firebase.google.com/docs/extensions/manage-installed-extensions#monitor) of your installed extension, including checks on its health, usage, and logs.
