import {Handlers} from '../functions/src/handlers';

export class JestMock<T> {
  fn: T | {[p: string]: jest.Mock} | any = {};
  typed = (this.fn as any) as T;

  constructor(...methodNames: string[]) {
    methodNames.forEach((name) => (this.fn[name] = jest.fn()));
  }
}

describe('Handlers', () => {
  let fireAuditMock: JestMock<any>;

  beforeEach(() => {
    fireAuditMock = new JestMock('writeHandler', 'pubsubHandler');
    process.env.API_KEY = 'TEST_API_KEY';
    process.env.DOMAIN = 'TEST_DOMAIN';
  });

  describe('onDocumentWrite', () => {
    it('calls fireAudit writeHandler with expected defaults', async () => {
      await Handlers.onDocumentWrite('TEST_CHANGE' as any, 'TEST_CONTEXT' as any, fireAuditMock.typed);

      expect(fireAuditMock.fn.writeHandler).toHaveBeenCalledWith('TEST_CHANGE', 'TEST_CONTEXT', {
        apiKey: 'TEST_API_KEY',
        domain: 'TEST_DOMAIN',
        disableDelete: false,
      });
    });

    it('calls fireAudit writeHandler with disableDelete when configured by process', async () => {
      process.env.DISABLE_DELETION = 'disabled';

      await Handlers.onDocumentWrite('TEST_CHANGE' as any, 'TEST_CONTEXT' as any, fireAuditMock.typed);

      expect(fireAuditMock.fn.writeHandler).toHaveBeenCalledWith('TEST_CHANGE', 'TEST_CONTEXT', {
        apiKey: 'TEST_API_KEY',
        domain: 'TEST_DOMAIN',
        disableDelete: true,
      });
    });
  });

  describe('onPublishExport', () => {
    it('calls fireAudit pubsubHandler with expected defaults', async () => {
      process.env.ROOT = 'TEST_ROOT';

      await Handlers.onPublishExport('TEST_MESSAGE', fireAuditMock.typed);

      expect(fireAuditMock.fn.pubsubHandler).toHaveBeenCalledWith('TEST_MESSAGE', {root: 'TEST_ROOT'});
    });
  });
});
