# FireAudit

With [FireAudit](https://fireaudit.studio) you have a complete studio to analyse the state of your data, and how this state came to be.

It's grown out of our own need to have insights into the history of documents.
Every once in a while, you see something odd with a document, but you have no clue what happened.
Or you have a functional requirement stating that your database needs to be auditable.
Firestore doesn't come with the tools to meet that requirement, until now!

# Setup

This extension creates functions that trigger on document writes, 8 levels of nesting from the root.

## Parameters

During installation, the process asks for an **API key** and the **Domain**. Make sure to go to https://fireaudi.studio, choose a domain, and obtain an API Key.

## Advanced

FireAudit offers more customisation via it's [client library](https://www.npmjs.com/package/@fireaudit/functions).
Additional customisations include any amount of data nesting, data encryption, and data transformation.

# Billing

This extension makes https calls to our services. Even though you can choose to use the free tier of FireAudit, you will be making https calls, for which Firebase required billing enabled.
Regardless of your payment plan with FireAudit, Firebase rates apply for resources consumed by the FireAudit Client Functions code.

## Firebase

This extension uses other Firebase or Google Cloud Platform services which may have associated charges:

- Cloud Functions
- Cloud Firestore

When you use Firebase Extensions, you're only charged for the underlying
resources that you use. A paid-tier billing plan is only required if the
extension uses a service that requires a paid-tier plan, for example calling to
a Google Cloud Platform API or making outbound network requests to non-Google
services. All Firebase services offer a free tier of usage.
[Learn more about Firebase billing.](https://firebase.google.com/pricing)

## FireAudit

Usage of this extension also requires you to have a FireAudit account.
You are responsible for any associated costs with your usage of FireAudit.
[Learn more about FireAudit billing.](https://fireaudit.studio)
