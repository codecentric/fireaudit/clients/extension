'use strict';
Object.defineProperty(exports, '__esModule', {value: true});
exports.Handlers = void 0;
class Handlers {
  static async onDocumentWrite(change, context, fireAudit) {
    await fireAudit.writeHandler(change, context, {
      apiKey: process.env.API_KEY,
      domain: process.env.DOMAIN,
      disableDelete: process.env.DISABLE_DELETION === 'disabled',
    });
  }
  static async onPublishExport(message, fireAudit) {
    await fireAudit.pubsubHandler(message, {root: process.env.ROOT});
  }
}
exports.Handlers = Handlers;
//# sourceMappingURL=handlers.js.map
