'use strict';
Object.defineProperty(exports, '__esModule', {value: true});
const functions = require('firebase-functions');
const handlers_1 = require('./handlers');
const fireaudit_1 = require('@fireaudit/functions/client/fireaudit');
// exports.FIRESTORE_EMULATOR_HOST = 'localhost:8080';
const fireAuditExportTrigger = functions.handler.pubsub.topic.onPublish((message) =>
  handlers_1.Handlers.onPublishExport(message, new fireaudit_1.FireAudit())
);
const fireAuditWriteTrigger = functions.handler.firestore.document.onWrite((change, context) =>
  handlers_1.Handlers.onDocumentWrite(change, context, new fireaudit_1.FireAudit())
);
exports.fireaudit1 = fireAuditWriteTrigger;
exports.fireaudit2 = fireAuditWriteTrigger;
exports.fireaudit3 = fireAuditWriteTrigger;
exports.fireaudit4 = fireAuditWriteTrigger;
exports.fireaudit5 = fireAuditWriteTrigger;
exports.fireaudit6 = fireAuditWriteTrigger;
exports.fireaudit7 = fireAuditWriteTrigger;
exports.fireaudit8 = fireAuditWriteTrigger;
exports.fireauditExport = fireAuditExportTrigger;
//# sourceMappingURL=index.js.map
