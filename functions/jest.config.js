module.exports = {
  roots: ['<rootDir>/src', '<rootDir>/../test'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testRegex: '.*spec.ts$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  coverageDirectory: './coverage',
  collectCoverageFrom: ['src/**/*.ts'],
  coveragePathIgnorePatterns: ['/src/index.ts'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
};
