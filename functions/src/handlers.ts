import {FireAudit} from '@fireaudit/functions/client/fireaudit';
import * as functions from 'firebase-functions';
import {DocumentSnapshot} from 'firebase-functions/lib/providers/firestore';

export class Handlers {
  static async onDocumentWrite(
    change: functions.Change<DocumentSnapshot>,
    context: functions.EventContext,
    fireAudit: FireAudit
  ): Promise<void> {
    await fireAudit.writeHandler(change, context, {
      apiKey: process.env.API_KEY,
      domain: process.env.DOMAIN,
      disableDelete: process.env.DISABLE_DELETION === 'disabled',
    } as any);
  }

  static async onPublishExport(message: any, fireAudit: FireAudit): Promise<void> {
    await fireAudit.pubsubHandler(message, {root: process.env.ROOT} as any);
  }
}
