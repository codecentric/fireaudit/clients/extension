import * as functions from 'firebase-functions';
import {Handlers} from './handlers';
import {FireAudit} from '@fireaudit/functions/client/fireaudit';

// exports.FIRESTORE_EMULATOR_HOST = 'localhost:8080';

const fireAuditExportTrigger = functions.handler.pubsub.topic.onPublish((message) => Handlers.onPublishExport(message, new FireAudit()));

const fireAuditWriteTrigger = functions.handler.firestore.document.onWrite((change, context) =>
  Handlers.onDocumentWrite(change, context, new FireAudit())
);

exports.fireaudit1 = fireAuditWriteTrigger;
exports.fireaudit2 = fireAuditWriteTrigger;
exports.fireaudit3 = fireAuditWriteTrigger;
exports.fireaudit4 = fireAuditWriteTrigger;
exports.fireaudit5 = fireAuditWriteTrigger;
exports.fireaudit6 = fireAuditWriteTrigger;
exports.fireaudit7 = fireAuditWriteTrigger;
exports.fireaudit8 = fireAuditWriteTrigger;
exports.fireauditExport = fireAuditExportTrigger;
