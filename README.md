[![pipeline status](https://gitlab.com/codecentric/fireaudit/clients/extension/badges/master/pipeline.svg)](https://gitlab.com/codecentric/fireaudit/clients/extension/-/commits/master)
[![coverage report](https://gitlab.com/codecentric/fireaudit/clients/extension/badges/master/coverage.svg)](https://gitlab.com/codecentric/fireaudit/clients/extension/-/commits/master)

# FireAudit Extension

Add Audit capabilities to Cloud Firestore. Track all documents to every document in time, analyse data quality and find relations.
See https://fireaudit.studio.

# FireAudit

Database auditing enables you to draw conclusions about actions that happened in the past, and prevent malicious activity since users know the can be held accountable for their actions. Combined with analyses, it can inform you of suspicious activity, and give insight into usage metrics.

By leveraging the reactive nature of Firestore, we created an environment that brings auditablity to all of our Firestore projects, and thus to any project's Firestore. Any mutation in a client's Firestore causes an append to FireAudit's BigQuery. This way, FireAudit always has the record on 'what happened when', and paired with meta information, it can also answer 'who and why'. Next to that, analyses bring violations to light, like data integrity and data tampering.

# Read more

- [FireAudit Studio](https://fireaudit.studio)
- [FireAudit Client Functions](https://www.npmjs.com/package/@fireaudit/functions)
